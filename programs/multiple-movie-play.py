#!/usr/bin/env python3

import math
import shlex
import sys
import subprocess

ffmpeg = "/usr/bin/ffmpeg"
ffplay = "/usr/bin/ffplay"

args = sys.argv[1:]
if len(args)>16:
    print("not support to play more than 16 movies")
    exit(1)

width = math.ceil(len(args)**0.5)
height = math.ceil(len(args)/width)

indexStr = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"]

fileStr = ""
for i in range(len(args)): # type: ignore
    fileStr += "-i " + args[i] + " "

filterStr = ""
for i, f in enumerate(args):
    filterStr += '[' + str(i) + ']' + \
                 "scale=w=1920:h=1080," + \
                 "drawtext=text='" + f + "':fontsize=40:fontcolor=gray[" + indexStr[i] + "];"
for i, f in enumerate(args):
    filterStr += "[" + indexStr[i] + "]"
filterStr += "xstack=inputs=" + str(len(args)) + ":layout="

cnt = 1
for h in range(height):
    wStr = ""
    hStr = ""
    if h>0:
        hhStr = ""
        for hh in range(h):
            hhStr += "h" + str(hh) + "+"
        hStr = hhStr[:-1]
    else:
        hStr = "0"
    for w in range(width):
        if w>0:
            wwStr = ""
            for ww in range(w):
                wwStr += "w" + str(ww) + "+"
            wStr = wwStr[:-1]
        else:
            wStr = "0"
        filterStr += wStr + "_" + hStr + "|"
        cnt += 1
        if(cnt>len(args)):
            break
    if(cnt>len(args)):
        break
filterStr = filterStr[:-1] + "[final]"
filterStr += ";[final]drawtext=text='%{frame_num}':x=(w-tw)/2:y=h-(2*lh):box=1:boxcolor=white:boxborderw=5:fontsize=40"

cmdStr = ffmpeg + " " + fileStr + " -filter_complex \"" + filterStr +"\" -c:v huffyuv -map 0:a:0 -c:a pcm_s16le -f matroska -"
subprocess.run(shlex.split(cmdStr))
#print(cmdStr)