#!/usr/bin/env python3

import datetime
import hashlib
import matplotlib.pylab as plt
from multiprocessing import Pool
import numpy as np
import os
import pandas as pd
import paramiko
import psutil
import re
import scp
import shlex
import subprocess
import sys
import datetime
import time
import xml.etree.ElementTree as ET

FFMPEG = "/usr/local/ffmpeg-6.0/ffmpeg"

def getDurationInSeconds(infile):
    cmdstr = FFMPEG + ' -i "'+infile+'"'
    timestr=""
    p = subprocess.Popen(shlex.split(cmdstr), stderr=subprocess.PIPE)
    try:
        outs, errs = p.communicate(timeout=5)
    except :
        p.kill()
        outs, errs = p.communicate()
    for line in errs.splitlines():
        line = line.decode("utf-8", errors='ignore')
        if line != "":
            if re.search(r'Duration:', line) != None:
                ptn = r'\w+:\w+:\w+.\w+'
                try:
                    timestr = re.search(ptn, line).group(0) # type: ignore
                except:
                    sys.stderr.write("FAILED: "+infile+"\n"+"FAILED: "+line)
                    timestr = ""
    if timestr != "":
        m = timestr.split('.')[0]
        x = time.strptime(m, '%H:%M:%S')
        secs = datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()
        return float(secs)
    else:
        return float(0)


def updateUsage(cpuTimes, prevCpuTimes):
    tmpCpuTimes = psutil.cpu_times()
    cpuTimes["user"] = cpuTimes["user"] + tmpCpuTimes.user - prevCpuTimes.user
    cpuTimes["system"] = cpuTimes["system"] + tmpCpuTimes.system - prevCpuTimes.system
    cpuTimes["nice"] = cpuTimes["nice"] + tmpCpuTimes.nice - prevCpuTimes.nice
    cpuTimes["idle"] = cpuTimes["idle"] + tmpCpuTimes.idle - prevCpuTimes.idle
    cpuTimes["iowait"] = cpuTimes["iowait"] + tmpCpuTimes.iowait - prevCpuTimes.iowait
    cpuTimes["irq"] = cpuTimes["irq"] + tmpCpuTimes.irq - prevCpuTimes.irq
    cpuTimes["softirq"] = cpuTimes["softirq"] + tmpCpuTimes.softirq - prevCpuTimes.softirq
    print(cpuTimes)
    return cpuTimes, tmpCpuTimes

#
# encode H.265 for baseline
#
def encodeH265(infile, tmpf, inbase, statf):
    outfile = workDir + "/" + tmpf + "_h265.mkv"
    if os.path.exists(outfile):
        print("encoded file already exists. skip:", outfile)
        return

    print("encode H.265 pass 1")
    # Pass 1 ---------------
    cmdstr = FFMPEG + " -y " + \
            "-i " + workDir + "/" + tmpf + ".mkv " + \
            " -c:v libx265 " + \
            " -preset slow " + \
            " -x265-params pass=1 " + \
            " -b:v 2000k "+ \
            " -strict -2 -an " + \
            " -f null /dev/null"
    starttime = datetime.datetime.now()
    p = subprocess.Popen(shlex.split(cmdstr))
    cnt = 0
    cpuTimes = {
        "user": 0.0,
        "system": 0.0,
        "nice": 0.0,
        "idle": 0.0,
        "iowait": 0.0,
        "irq": 0.0,
        "softirq": 0.0,
    }
    prevCpuTimes = psutil.cpu_times()
    while True:
        if p.poll() is not None:
            break
        time.sleep(1)
        cnt += 1
        if cnt % 60 == 0:
            cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
    os.sync()
    encodetime = datetime.datetime.now() - starttime
    cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)

    # Pass 2 ---------------
    print("encode H.265 pass 2")
    cmdstr = FFMPEG + " -y " + \
            "-i " + workDir + "/" + tmpf + ".mkv " + \
            " -map 0:v:0 -c:v libx265 " + \
            " -preset slow " + \
            " -x265-params pass=2 " + \
            " -b:v 2000k "+ \
            " -strict -2" + \
            ' -map 0:a:0 -c:a aac -b:a 144k -ar 48000 -ac 2 ' + \
            workDir + "/" + tmpf + "_h265.mkv"
    starttime = datetime.datetime.now()
    p = subprocess.Popen(shlex.split(cmdstr))
    cnt = 0
    prevCpuTimes = psutil.cpu_times()
    while True:
        if p.poll() is not None:
            break
        time.sleep(1)
        cnt += 1
        if cnt % 60 == 0:
            cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
    os.sync()
    encodetime = encodetime + (datetime.datetime.now() - starttime)
    cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
    print(infile, "H.265 encoded finished. time:", encodetime)
    filesize = os.path.getsize(workDir + "/" + tmpf + "_h265.mkv")

    s = '{0:s},{1:s},{2:02d},{3:02d},{4:02d},{5:0d},{6:.03f},{7:d},{8:.02f},{9:.02f},{10:.02f},{11:.02f},{12:.02f},{13:.02f},{14:.02f}'.format(
        re.search(r'(' + sourceDir + '/)(\S+)', inbase).group(2), # type: ignore
        "H.265",
        6,
        28,
        0,
        0,
        encodetime.total_seconds(),
        filesize,
        cpuTimes["user"], # type: ignore
        cpuTimes["system"], # type: ignore
        cpuTimes["nice"], # type: ignore
        cpuTimes["idle"], # type: ignore
        cpuTimes["iowait"], # type: ignore
        cpuTimes["irq"], # type: ignore
        cpuTimes["softirq"], # type: ignore
    )+"\n"
    if not os.path.isfile(statf):
        fh = open(statf, "a")
        fh.write("source,codec,preset,crf,filmgrain,tune,encodetime,filesize,cpu_user,cpu_system,cpu_nice,cpu_idle,cpu_iowait,cpu_irq,cpu_softirq\n")
        fh.close()
        os.sync()
    fh = open(statf, "a")
    fh.write(s)
    fh.close()
    os.sync()

def encodeMovie(infile):
    nt = datetime.datetime.now()
    print(nt, "Encode task start:", infile)
    inbase, inext = os.path.splitext(infile)
    tmpf = hashlib.sha1(infile.encode('utf-8')).hexdigest()
    statf = inbase+".csv"

    #
    # tranport source file to localhost
    #
    if os.path.exists(infile+".touch"):
        print("found touch file. another process may work with", infile+".touch")
        return
    try:
        fh = open(infile+".touch", "w")
        fh.write('')
        fh.close()
        os.sync()
    except:
        print("cannot create touch file", infile+".touch")
        return
    try:
        with paramiko.SSHClient() as sshc:
            sshc.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            sshc.connect(hostname="192.168.111.111", port=22, username="yourname", password='yourpassword')
            with scp.SCPClient(sshc.get_transport()) as scpc:  # type: ignore
                scpc.get(
                    remote_path=infile,
                    local_path=workDir + "/" + tmpf + inext
                )
    except:
        print("file has vanished", infile)
        os.remove(infile+".touch")
        return

    #
    # generate intermediate mkv file
    #
    if os.path.exists(workDir + "/" + tmpf + ".mkv"):
        print("intermediate mkv file already exists. skip.")
    else:
        print("generating intermediate mkv file...")
        cmdstr3 = FFMPEG + \
                " -i " + workDir + "/" + tmpf + inext + \
                " -map 0:v:0" + \
                " -c:v huffyuv" + \
                " -vf yadif=0:-1:1" + \
                " -map 0:a:0" + \
                " -c:a pcm_s16le -ac 2" + \
                " " + workDir + "/" + tmpf + ".mkv"
        p = subprocess.Popen(shlex.split(cmdstr3))
        p.wait()
        os.sync()

    #
    # encode AV1 file
    #
    def encodeAV1(infile, av1preset, av1crf, av1filmgrain, av1tune):
        print("encode av1 preset " + str(av1preset) + "...")
        sourceFile = workDir + "/" + tmpf + ".mkv"
        interFile = workDir + "/" + tmpf + \
                  "_av1-p" + str(av1preset) + "crf" + str(av1crf) + \
                  "filmg" + str(av1filmgrain) + "tune" + str(av1tune) + ".ivf"
        statFile = workDir + "/" + tmpf + \
                  "_av1-p" + str(av1preset) + "crf" + str(av1crf) + \
                  "filmg" + str(av1filmgrain) + "tune" + str(av1tune) + ".stat"
        outfile = workDir + "/" + tmpf + \
                "_av1-p" + str(av1preset) + "crf" + str(av1crf) + \
                "filmg" + str(av1filmgrain) + "tune" + str(av1tune) + ".mp4"

        if os.path.exists(outfile):
            print("encoded file already exists. skip:", outfile)
            return

        # Pass 1
        cmdstr1 = FFMPEG + " " + \
                "-i " + sourceFile + " " + \
                "-map 0:v:0 -pix_fmt yuv420p10le -s 1920x1080 " + \
                "-f yuv4mpegpipe -strict -1 -"
        cmdstr2 = "/usr/local/bin/SvtAv1EncApp --input stdin " + \
                  "--width 1920 --height 1080 " + \
                  "--preset " + str(av1preset) + " " + \
                  "--rc 1 --tbr 1000 --mbr 2000 " + \
                  "--film-grain " + str(av1filmgrain) + " " + \
                  "--tune " + str(av1tune) + " " + \
                  "--keyint 30 " + \
                  "--pass 1 --stats " + statFile + " " + \
                  "--output " + \
                  interFile
        starttime = datetime.datetime.now()
        p1 = subprocess.Popen(shlex.split(cmdstr1), shell=False, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(shlex.split(cmdstr2), shell=False, stdin=p1.stdout)
        cpuTimes = {
            "user": 0.0,
            "system": 0.0,
            "nice": 0.0,
            "idle": 0.0,
            "iowait": 0.0,
            "irq": 0.0,
            "softirq": 0.0,
        }
        prevCpuTimes = psutil.cpu_times()
        cnt = 0
        while True:
            if p1.poll() is not None and p2.poll() is not None:
                break
            time.sleep(1)
            cnt += 1
            if cnt % 60 == 0:
                cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
        os.sync()
        # Pass 2
        cmdstr1 = FFMPEG + " " + \
                "-i " + sourceFile + " " + \
                "-map 0:v:0 -pix_fmt yuv420p10le -s 1920x1080 " + \
                "-f yuv4mpegpipe -strict -1 -"
        cmdstr2 = "/usr/local/bin/SvtAv1EncApp --input stdin " + \
                  "--width 1920 --height 1080 " + \
                  "--preset " + str(av1preset) + " " + \
                  "--rc 1 --tbr 1000 --mbr 2000 " + \
                  "--film-grain " + str(av1filmgrain) + " " + \
                  "--tune " + str(av1tune) + " " + \
                  "--keyint 30 " + \
                  "--pass 2 --stats " + statFile + " " + \
                  "--output " + \
                  interFile
        p1 = subprocess.Popen(shlex.split(cmdstr1), shell=False, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(shlex.split(cmdstr2), shell=False, stdin=p1.stdout)
        prevCpuTimes = psutil.cpu_times()
        cnt = 0
        while True:
            if p1.poll() is not None and p2.poll() is not None:
                break
            time.sleep(1)
            cnt += 1
            if cnt % 60 == 0:
                cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
        os.sync()
        # merge audio
        cmdstr3 = FFMPEG + " " + \
                "-i " +  interFile + " " + \
                "-i " + sourceFile + " " + \
                "-map 0:v:0 -c:v copy -map 1:a:0 -c:a aac -b:a 144k -ac 2 " + \
                outfile
        p3 = subprocess.Popen(shlex.split(cmdstr3), shell=False)
        cnt = 0
        while True:
            if p3.poll() is not None:
                break
            time.sleep(1)
            cnt += 1
            if cnt % 60 == 0:
                cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
        os.remove(interFile)
        os.remove(statFile)
        os.sync()
        encodetime = datetime.datetime.now() - starttime
        cpuTimes, prevCpuTimes = updateUsage(cpuTimes, prevCpuTimes)
        filesize = os.path.getsize(outfile)
        print(infile, "AV1 encoded finished. time:", encodetime)

        s = '{0:s},{1:s},{2:02d},{3:02d},{4:02d},{5:0d},{6:.03f},{7:d},{8:.02f},{9:.02f},{10:.02f},{11:.02f},{12:.02f},{13:.02f},{14:.02f}'.format(
            re.search(r'(' + sourceDir + '/)(\S+)', inbase).group(2), # type: ignore
            "SVT-AV1",
            av1preset,
            av1crf,
            av1filmgrain,
            av1tune,
            encodetime.total_seconds(),
            filesize,
            cpuTimes["user"], # type: ignore
            cpuTimes["system"], # type: ignore
            cpuTimes["nice"], # type: ignore
            cpuTimes["idle"], # type: ignore
            cpuTimes["iowait"], # type: ignore
            cpuTimes["irq"], # type: ignore
            cpuTimes["softirq"], # type: ignore
        )+"\n"
        if not os.path.isfile(statf):
            fh = open(statf, "a")
            fh.write("source,codec,preset,crf,filmgrain,tune,encodetime,filesize,cpu_user,cpu_system,cpu_nice,cpu_idle,cpu_iowait,cpu_irq,cpu_softirq\n")
            fh.close()
            os.sync()
        fh = open(statf, "a")
        fh.write(s)
        fh.close()
        os.sync()

    encodeH265(infile, tmpf, inbase, statf)
    for av1preset in av1presets:
        for av1crf in av1crfs:
            for av1filmgrain in av1filmgrains:
                for av1tune in av1tunes:
                    encodeAV1(infile, av1preset, av1crf, av1filmgrain, av1tune)

    os.remove(workDir + "/" + tmpf + inext)
    return

# VMAF計算
def generateVMAF(attr):
    originalFile = attr["originalFile"]
    eval = attr["eval"]
    targetFile = eval["file"]

    #if eval["codec"] == "H.265":
    #    return

    # VMAFを算出
    if os.path.exists(targetFile + ".vmaff.xml"):
        # 比較データがすでにあったらスキップ
        print(eval["codec"], "Preset", eval["preset"], "CRF", eval["crf"], ":",
            "evaluated result", targetFile + ".vmaff.xml",
            "already exists. skip re-evaluate.")
    else:
        cmdstr = FFMPEG + " " + \
                "-i " + originalFile + " " + \
                "-i " + targetFile + " " + \
                "-map 0:v:0 -map 1:v:0 " + \
                "-filter_complex \"[0]scale=w=1920:h=1080[ref];[1]scale=w=1920:h=1080[main];" + \
                "[main][ref]libvmaf=log_path=" + targetFile + ".vmaff.xml\" -f null /dev/null"
        print(cmdstr)
        p = subprocess.Popen(shlex.split(cmdstr))
        p.wait()
        os.sync()
        
        cmdstr = "sed -e \"1i <?xml version=\\\"1.0\\\"?>\" " + targetFile + ".vmaff.xml"
        p = subprocess.Popen(shlex.split(cmdstr), stdout=subprocess.PIPE)
        fn = targetFile + ".vmaff.xml2"
        f = open(fn, "a")
        while True:
            if p.poll() is not None:
                break
            outs, errs = p.communicate()
            f.write(outs.decode('utf-8'))
        f.close()
        os.sync()
        os.rename(targetFile + ".vmaff.xml2",
                    targetFile + ".vmaff.xml")
    # 結果のXMLファイルから値を取得
    vmafScore = 0.0
    vmafXML = ET.parse(targetFile + ".vmaff.xml")
    r = vmafXML.getroot()
    for node in r.iter('metric'):
        if node.attrib["name"] == "vmaf":
            vmafScore = float(node.get("mean")) # type: ignore
    frameVMAFs = []
    for node in r.iter('frame'):
        frameVMAFs.append(float(node.get('vmaf'))) # type: ignore
    return {
        "total": {
            "codec": eval["codec"],
            "preset": eval["preset"],
            "crf": eval["crf"],
            "vmafScore": float(vmafScore)/100
        },
        "frameVMAF": {
            "name": '{0:s}-p{1:02d}-crf{2:02d}-filmg{3:02d}-tune{4:01d}'.format(
                    eval["codec"],
                    eval["preset"],
                    eval["crf"],
                    eval["filmgrain"],
                    eval["tune"]
            ),
            "vmaf": frameVMAFs
        }
    }

def calcurateVMAF(infile):
    nt = datetime.datetime.now()
    print(nt, "Evaluate task start:", infile)
    
    inbase, inext = os.path.splitext(infile)
    tmpf = hashlib.sha1(infile.encode('utf-8')).hexdigest()

    # 比較元、対象のファイルをチェックし、条件を満たしている場合は比較処理対象に追加
    patterns = {
        "original": {
            "file": workDir + "/" + tmpf + ".mkv"
        },
        "evaluation": [{
            "codec": "H.265",
            "preset": 6, # slow
            "crf": 28,   # default
            "filmgrain": 0,
            "tune": 0,
            "file": workDir + "/" + tmpf + "_h265.mkv"
        }]
    }
    for av1preset in av1presets:
        for av1crf in av1crfs:
            for av1filmgrain in av1filmgrains:
                for av1tune in av1tunes:
                    file = workDir + "/" + tmpf + \
                            "_av1-p" + str(av1preset) + "crf" + str(av1crf) + \
                            "filmg" + str(av1filmgrain) + "tune" + str(av1tune) + ".mp4"
                    if not os.path.exists(file):
                        print("cannot find encoded file. skip:", av1preset, av1crf, file, av1filmgrain, av1tune)
                    else:
                        patterns["evaluation"].append({
                            "codec": "SVT-AV1",
                            "preset": av1preset,
                            "crf": av1crf,
                            "filmgrain": av1filmgrain,
                            "tune": av1tune,
                            "file": file
                        })
    if not os.path.exists(patterns["evaluation"][0]["file"]):
        print("minimam baseline encoded file not found. VMAF evaluation is aborted.")
        return


    attrs = []
    for e in patterns["evaluation"]:
        attrs.append({
            "originalFile": patterns["original"]["file"],
            "eval": e
        })
    with Pool(15) as p:
        retarray = p.map(generateVMAF, attrs)
    
    # フレームごとのVMAFの遷移をチャート化
    totalScores = []
    frameVMAFs = []
    frameVMAFindex = []
    for r in retarray:
        if r is None:
            continue
        totalScores.append(r["total"])
        frameVMAFindex.append(r["frameVMAF"]["name"])
        frameVMAFs.append(r["frameVMAF"]["vmaf"])
    df = pd.DataFrame(frameVMAFs).transpose().set_axis(frameVMAFindex, axis=1)
    ax = df.plot(figsize=(100.0, 4.0), linewidth=0.1)
    ax.set_xticks(np.arange(0, len(frameVMAFs[0]), 200))
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(5)
    ax.set_yticks(np.arange(0, 100, 5), labelsize=5)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(8)

    ax.grid(axis='both', which='both', linewidth=0.5)
    ax.legend(loc='best', fontsize=6)
    plt.savefig(infile+"frameVMAF-all.png", dpi=300)
    
    # CPU利用効率を追加
    vmafScores = pd.DataFrame(totalScores)
    statf = inbase+".csv"
    if not os.path.exists(statf):
        print("stat CSV file doesn't exist. abort.")
        return
    df = pd.read_csv(statf)
    usedCPU = df["cpu_user"] + df["cpu_system"] + df["cpu_nice"] + df["cpu_iowait"] + df["cpu_irq"] + df["cpu_softirq"]
    allCPU = usedCPU + df["cpu_idle"]
    df["cpu_efficiency"] = (usedCPU/16) / (allCPU/16)
    # VMAFスコアをCSV ファイルに反映
    df = pd.read_csv(statf)
    if not "vmafScore" in df.columns:
        pd.merge(df, vmafScores, how='inner').to_csv(statf)


#-------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------

sourceDir = "/home/youname/enctest"
workDir = "/home/yourname/enctmp"
# default - see https://gitlab.com/AOMediaCodec/SVT-AV1/-/blob/master/Docs/CommonQuestions.md#what-presets-do
av1presets = [12,11,10,9,8,6,5,4,2] # 
av1crfs = [63,53,43,33,23,13,3]
av1filmgrains = [0,10,20,30,40,50]
av1tunes = [0,1]
# 検証1
#av1presets = [12,11,10,9,8,6,5,4,2]
#av1crfs = [0]
#av1filmgrains = [0,10,20,30,40,50]
#av1tunes = [0,1]
# 検証2
#av1presets = [5,2]
#av1crfs = [0]
#av1filmgrains = [0,50]
#av1tunes = [0,1]

# ロックファイルのチェック
if os.path.exists("/var/tmp/tsmigrate.lock"):
    print("another tsmigrate process is running. exit.")
    exit(1)
else:
    f = open("/var/tmp/tsmigrate.lock", "w")
    f.write("lock")
    f.close()

# ソースファイルの検索
inputar = []
proc = subprocess.Popen(["find",
                         sourceDir + "/",
                         "-type", "f",
                         "-name", "*.ts"],
                         shell=False, stdout=subprocess.PIPE)
while True:
  time.sleep(1e-4)
  line = proc.stdout.readline().rstrip()  # type: ignore
  if len(line) >0:
     inputar.append(line.decode('utf-8'))
  if proc.poll() is not None:
     break

# 見つかったファイルのなかから除外対象を省く
numenc = 0
totalsec = float(0.0)
tgtar = []
for infile in inputar:
    infile = infile.rstrip()

    # 対象ファイルがあるディレクトリに .tsmigrate.ignore があったらスキップ
    skipdir = False
    if os.path.exists(os.path.dirname(infile) + "/.tsmigrate.ignore"):
        skipdir = True
    
    # 以下の条件にあてはまったらスキップ
    # 2). .info, .queued ファイルがある
    infofile = infile + ".info"
    qfile = infile + ".queued"
    isInfo = os.path.exists(infofile)
    isQueue = os.path.exists(qfile)
    if isInfo == False      and \
       isQueue == False     and \
       skipdir == False:
       numenc += 1
       secs = float(getDurationInSeconds(infile))
       totalsec += secs
       tgtar.append(infile)
       print("append", infile)
    else:
       print("not target", infile)
       continue

# 対象ファイルの合計時間を算出
secs = int(totalsec%60)
hours = int((totalsec-secs)/(60*60))
mins = int(((totalsec-secs) - (hours*60*60))/60)
print("num of movies to be encoded: ", len(tgtar))
print("total movie duration: ", str(totalsec), "seconds",
      "{0:02d}:{1:02d}:{2:02d}".format(hours, mins, secs))

for i in tgtar:
    encodeMovie(i)
    calcurateVMAF(i)
os.remove("/var/tmp/tsmigrate.lock")

